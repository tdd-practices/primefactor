package hu.webvalto.primefactor;


import org.junit.Before;
import org.junit.Test;

import java.util.Collections;

import static org.junit.Assert.*;

public class PrimefactorTest {

    private PrimeFactorCalculator testObj;

    @Before
    public void setup() {
        testObj = new PrimeFactorCalculator();
    }

    @Test
    public void should_calculate_for_one() {
        assertEquals(Collections.emptyList(),
                testObj.generate(1));
    }

}
